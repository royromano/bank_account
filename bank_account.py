import time
from datetime import datetime
class BankAccount:
    def __init__(self, name: str, id: str, balance: float, commission=0.1):
        self.name = name
        self.id = id
        self.balance = balance
        self.commission = commission
        self.history = {}

    def __str__(self):
        return f"this is the bank account of {self.name} and he have {self.balance}"
    def set_commission(self):
        pass

    def withdraw(self, number:int):
        commi = number + number * self.commission
        if self.balance >= commi:
            self.balance = self.balance - commi
            self.add_history(f"withdraw {number}")
            return True
        else:
            return False

    def deposit(self, number:int | float):
        self.balance += (number - number*self.commission)
        self.add_history(f"deposit {number}")
        return True


    def check_balance(self):
        return self.balance

    def add_history(self, action):
        self.history[datetime.now().strftime("%H:%M:%S.%f")[:-3]] = action

    def recent_actions(self):
            return self.history

    def transfer(self, to_who, how_much:float)->bool:
        if self.withdraw(how_much):
            to_who.deposit(how_much)
            return True
        else:
            return False










if __name__ == "__main__":
    account1 = BankAccount("Roy Romano", 1000, 25000)
    print(account1)
    account1.withdraw(50000)
    time.sleep(1)
    account1.deposit(100000)
    time.sleep(1)
    account1.check_balance()
    print(account1.recent_actions())
    account1.withdraw(50000)
    time.sleep(1)
    print(account1.recent_actions())

