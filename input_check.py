def get_numeric(user_input):
    while True:
        if user_input.isnumeric():
            numeric_value = float(user_input)
            return numeric_value
        else:
            user_input = input("Invalid input. Please enter a numeric value: ")
