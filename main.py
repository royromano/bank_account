from bank_account import BankAccount
from input_check import get_numeric
start_account = [{"name":"Roy","id":"8","balance":700},{"name":"yakir","id":"9","balance":700},{"name":"tamir","id":"81","balance":700}]
bank_accounts = []

for account in start_account:
    a= BankAccount(**account)
    bank_accounts.append(a)

actions = (f"please select an action\n1.account create\n2.select account\n3.deposit\n4.withdraw\n5.log\n6.transfer\n7.exit")
current_account = None
# name: str, id: int, balance: float
def create_account():
    global bank_accounts
    name = input("enter name: ")
    id = input("enter id: ")
    balance = get_numeric(input("enter balance: "))
    account = BankAccount(name, id, balance)
    bank_accounts.append(account)
    print("success")


def select_account()->BankAccount:
    global current_account
    current_id = input("enter id: ")
    check = False
    for account in bank_accounts:
        if account.id == current_id:
            check = True
            current_account = account
            print(f"the account of {current_account.name} is your current account")
            break
    if check is False:
        print("account not found")



def deposit():
    global current_account
    depo = get_numeric(input("enter how much to depose: "))
    if current_account.deposit(depo):
        print(f"deposit succeed! your balance is: {current_account.balance}")
    else:
        print("somthing went wrong")


def withdraw():
    global current_account
    draw = float(input("enter how much you want to withdraw: "))
    if current_account.withdraw(draw):
        print(f"withdraw succeed! your new balance is: {current_account.balance}")
    else:
        print("you dont have enough money...")


def log():
    global current_account
    hist = current_account.recent_actions()
    if not hist:
        print("no history for this account")
    else:
        print(hist)

def find(id):
    for account in bank_accounts:
        if account.id == id:
            return account
    return None


def transfer():
    global current_account
    to_who = find(input("enter id to transfer "))
    if to_who is not None:
        how_much = get_numeric(input("how much to transfer? "))
        if current_account.transfer(to_who, how_much):
            print(f"you transferred {how_much} to {to_who.name}")
        else:
            print("you dont have enough money")
    else:
        print("didn't found an account for the id")

def check_current()->bool:
    global current_account
    if current_account is None:
        print("please select account first")
        return False
    return True



while True:
    print(actions)
    action = input("what would you like to do? ")
    if action == "1":
        create_account()
    elif action == "2":
        select_account()
    elif action == "3":
        if check_current():
            deposit()
    elif action == "4":
        if check_current():
            withdraw()
    elif action == "5":
        if check_current():
            log()
    elif action == "6":
        if check_current():
            transfer()
    elif action == "7":
        print("bye")
        break
    else:
        print("invalid option")








